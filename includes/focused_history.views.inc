<?php

/**
 * @file
 * Focused History views data definitions.
 */

/**
 * Implements hook_views_data().
 **/
function focused_history_views_data() {
  $data = array();

  $data['focused_history']['table']['group'] = t('Focused History');

  $data['focused_history']['sid'] = array(
    'title' => t('Session API sid'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['focused_history']['nid'] = array(
    'title' => t('Nid'),
    'help' => t('Focused History nid'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['focused_history']['timestamp'] = array(
    'title' => t('Last viewed'),
    'help' => t('Focused History Datetime'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function focused_history_views_data_alter(&$data) {
  // Expose a relationship to the focused_history table.
  $data['node']['focused_history'] = array(
    'group' => t('Focused History'),
    'title' => t('Focused History'),
    'help' => t('Relate a node to its browsing history recorded by focused history.'),
    'relationship' => array(
      'label' => t('Focused History'),
      'base' => 'focused_history',
      'base field' => 'nid',
      'relationship field' => 'nid',
      'handler' => 'views_handler_relationship'
    ),
  );

  // Expose a relationship to the user in the focused_history table.
  $data['focused_history']['user'] = array(
    'group' => t('Focused History'),
    'title' => t('User'),
    'help' => t('Relate a focused history result to its user.'),
    'relationship' => array(
      'label' => t('Focused History User'),
      'base' => 'users',
      'base field' => 'uid',
      'relationship field' => 'uid',
      'handler' => 'views_handler_relationship'
    ),
  );
}
