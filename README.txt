Focused History
===============

Description
-----------

Focused History records content browsing history with a more customizable degree of specificity than the history and accesslog (statistics module) tables. It provides a basic administrative view of this more 'focused' history.

The most generally used options for recording browsing history in the database have some major drawbacks:

1) these database tables can get massively bloated in high traffic sites
2) some don't provide records for anonymous users

This module would be particularly helpful for high-traffic sites that, for example, want to build content recommendations based on user browsing history but don't want to maintain a massive browsing history table.

Focused History allows admins to remove the cruft from recorded browsing history by:

- including only specific content types
- including only specific view modes
- excluding specific user roles
- excluding specific IP addresses
- excluding known bots


Similar modules
---------------

Statistics -- provides 'accesslog' functionality to track views of all pages to your site.
Recently Read -- records browsing history of different enteties


Installation and Configuration
------------------------------

Nothing special about installation. The normal Drupal way:

1) Download Focused History to your modules directory.
2) Activate Focused History in administer >> modules.

Configure options at /admin/config/system/focused_history.


Contributors
------------
Sean Casey http://seanjcasey.com/
Ian Campbell
